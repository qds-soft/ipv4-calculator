class Ip
{
    constructor(ip, netmask)
    {
        this.ip = ip
        this.netmask = netmask
        this.#validateNetmask()
        this.#validateIp()
    }

    #toBin(int_decNum)
    {
       return int_decNum.toString(2)
    }

    #toDec(str_binNum)
    {
        return parseInt(str_binNum, 2)
    }

    #validateIp()
    {
        if(this.ip[0] > 255 || this.ip[0] < 0)
        {
            throw new Error('Erste Zahl der IP darf nicht größer als 255  oder kleiner als 0 sein!')
            return false
        }

        if(this.ip[1] > 255 || this.ip[1] < 0)
        {
            throw new Error('Zweite Zahl der IP darf nicht größer als 255 oder kleiner als 0 sein!')
            return false
        }

        if(this.ip[2] > 255 || this.ip[2] < 0)
        {
            throw new Error('Dritte Zahl der IP darf nicht größer als 255 oder kleiner als 0 sein!')
            return false
        }

        if(this.ip[3] > 255 || this.ip[3] < 0)
        {
            throw new Error('Vierte Zahl der IP darf nicht größer als 255 oder kleiner als 0 sein!')
            return false
        }
    }

    #validateNetmask()
    {
        if(this.netmask[0] > 255 || this.netmask[0] < 0)
        {
            throw new Error('Erste Zahl der Subnetzmaske darf nicht größer als oder kleiner als 0 255 sein!')
            return false
        }

        if(this.netmask[1] > 255 || this.netmask[1] < 0)
        {
            throw new Error('Zweite Zahl der Subnetzmaske darf nicht größer als 255 oder kleiner als 0 sein!')
            return false
        }

        if(this.netmask[2] > 255 || this.netmask[2] < 0)
        {
            throw new Error('Dritte Zahl der Subnetzmaske darf nicht größer als 255 oder kleiner als 0 sein!')
            return false
        }

        if(this.netmask[3] > 255 || this.netmask[3] < 0)
        {
            throw new Error('Vierte Zahl der Subnetzmaske darf nicht größer als 255 oder kleiner als 0 sein!')
            return false
        }

        let bin_1 = this.#toBin(this.netmask[0])
        let bin_2 = this.#toBin(this.netmask[1])
        let bin_3 = this.#toBin(this.netmask[2])
        let bin_4 = this.#toBin(this.netmask[3])

        let octet_1 = "00000000".substr(bin_1.length) + bin_1
        let octet_2 = "00000000".substr(bin_2.length) + bin_2
        let octet_3 = "00000000".substr(bin_3.length) + bin_3
        let octet_4 = "00000000".substr(bin_4.length) + bin_4

        var complete_netmask = octet_1 + octet_2 + octet_3 + octet_4
        if(complete_netmask.includes('01'))
        {
            throw new Error('Ungültige Netzmaske!')
            return false
        }
        return [octet_1,octet_2,octet_3,octet_4]
    }

    getBinIp()
    {
        let bin_1 = this.#toBin(this.ip[0])
        let bin_2 = this.#toBin(this.ip[1])
        let bin_3 = this.#toBin(this.ip[2])
        let bin_4 = this.#toBin(this.ip[3])

        let octet_1 = "00000000".substr(bin_1.length) + bin_1
        let octet_2 = "00000000".substr(bin_2.length) + bin_2
        let octet_3 = "00000000".substr(bin_3.length) + bin_3
        let octet_4 = "00000000".substr(bin_4.length) + bin_4

        return [octet_1,octet_2,octet_3,octet_4]
    }

    getBinNetmask()
    {
        return this.#validateNetmask()
    }

    getSlash()
    {
        let complete_netmask = this.getBinNetmask()[0] + this.getBinNetmask()[1] + this.getBinNetmask()[2] + this.getBinNetmask()[3]
        return (complete_netmask.match(/1/g) || []).length
    }

    getNetId()
    {
        return [(this.ip[0] & this.netmask[0]), (this.ip[1] & this.netmask[1]), (this.ip[2] & this.netmask[2]), (this.ip[3] & this.netmask[3])]
    }

    getBinNetId()
    {
        let bin_1 = this.#toBin(this.ip[0] & this.netmask[0])
        let bin_2 = this.#toBin(this.ip[1] & this.netmask[1])
        let bin_3 = this.#toBin(this.ip[2] & this.netmask[2])
        let bin_4 = this.#toBin(this.ip[3] & this.netmask[3])

        let octet_1 = "00000000".substr(bin_1.length) + bin_1
        let octet_2 = "00000000".substr(bin_2.length) + bin_2
        let octet_3 = "00000000".substr(bin_3.length) + bin_3
        let octet_4 = "00000000".substr(bin_4.length) + bin_4

        return [octet_1,octet_2,octet_3,octet_4]
    }

    getBinBroadcastId()
    {
        let net_id = this.getBinNetId()
        var complete_net_id = net_id[0] + net_id[1] + net_id[2] + net_id[3]

        for(let i = this.getSlash(); i < 32; i++)
        {
            complete_net_id = complete_net_id.substring(0, i) + '1' + complete_net_id.substring(i + 1);
        }

        return [complete_net_id.substring(0,8), complete_net_id.substring(8,16), complete_net_id.substring(16,24), complete_net_id.substring(24)]
    }

    getBroadcastId()
    {
        let dec_1 = this.#toDec(this.getBinBroadcastId()[0])
        let dec_2 = this.#toDec(this.getBinBroadcastId()[1])
        let dec_3 = this.#toDec(this.getBinBroadcastId()[2])
        let dec_4 = this.#toDec(this.getBinBroadcastId()[3])

        return [dec_1, dec_2, dec_3, dec_4]
    }

    getBinFirstHostId()
    {
        let first_host = this.getBinNetId()
        let last_tupel = this.#toBin(this.getNetId()[3] + 1)
        last_tupel = "00000000".substr(last_tupel.length) + last_tupel

        return [first_host[0], first_host[1], first_host[2], last_tupel]
    }

    getFirstHostId()
    {
        let first_host = this.getNetId()

        return [first_host[0], first_host[1], first_host[2], first_host[3] + 1]
    }

    getBinLastHostId()
    {
        let first_host = this.getBinBroadcastId()
        let last_tupel = this.#toBin(this.getBroadcastId()[3] - 1)
        last_tupel = "00000000".substr(last_tupel.length) + last_tupel

        return [first_host[0], first_host[1], first_host[2], last_tupel]
    }

    getLastHostId()
    {
        let first_host = this.getBroadcastId()

        return [first_host[0], first_host[1], first_host[2], first_host[3] - 1]
    
    }

    getHostsCount()
    {
        return Math.pow(2, (32 - this.getSlash())) -2
    }
}