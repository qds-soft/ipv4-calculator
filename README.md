# Verwendung

Da es sich hierbei um eine html-Datei mit JavaScript handelt, die keine Verbindung zum Internet aufbaut, kann sie lokal im Browser geöffnet werden.

_______________________________________________

This is a html file with JavaScript and can be used localy. Just open it with your browser.

## Funktion

Es kann eine IPv4 und eine Netzmaske eingegeben werden. Daraus wird folgendes berechnet:

- Binärdarstellung der IP
- Binärdarstellung der Netzmaske
- Slashnotation der IP
- Netz-ID binär und dezimal
- Broadcast-ID binär und dezimal
- Erster möglicher Host im Netz binär und dezimal
- Letzter möglicher Host im Netz binär und dezimal
- Anzahl möglicher Hosts im Netz

## Lizenz

CC BY-SA 3.0
https://creativecommons.org/licenses/by-sa/3.0/de/
